var userColor = '#00000'
var userName = ''
var userGroup = 1

var socket = io()

// 赋值监听
$('#nickname').on('change', e => {
    userName = e.target.value
})

$('#room-number').on('change', e => {
    userGroup = e.target.value
})

$('#add-or-join').click(function () {
    socket.emit('registerUser', { userName, userGroup })
})

$('#clear-msg').click(function () {
    $('#message').html('')
})

$('#send').click(sendMsg)

function sendMsg() {
    socket.emit('chatmessage', { userName, userGroup, userColor, msg: $('#msg').val() })
    $('#msg').val('')
    return false
}

socket.on('chatmessage', function (data) {
    if (data.userName === userName) {
        data.userName = '我'
    }
    $('#message').append($(`<li style="color:${data.userColor}">`).text(`${data.userName}: ${data.msg}`))

    scrollToBottom()

    if (data.userName !== '我') {
        notifyNewMsg()
    }
})

socket.on('userjoin', function (data) {
    if (data.userName !== userName) {
        $('#message').append($(`<li style="color:#d6d6d6; font-style: italic">`).text(`${data.userName} joined chatroom.`))
    } else {
        $('#message').append($(`<li style="color:#d6d6d6; font-style: italic">`).text(`Joined chatroom [${userGroup}] successfully.`))
    }

    scrollToBottom()
})

socket.on('userleave', function (data) {
    if (data.userName !== userName) {
        $('#message').append($(`<li style="color:#d6d6d6; font-style: italic">`).text(`${data.userName} leaved chatroom.`))
    }

    scrollToBottom()
})

socket.on('totalVisited', function (data) {
    $('#total-visited').html(`共计访问 ${data} 次`)
})

// 滚动条滚动到最下面
function scrollToBottom() {
    var scrollHeight = $('#message').prop('scrollHeight');
    $('#message').scrollTop(scrollHeight, 200);
}

// select word color
$('.color-item').click(e => {
    const $item = $(e.target)
    userColor = $item.css('background-color')
    setItemSelected($item)
})

// customize color
$('.color-item-picker').on('change input click', e => {
    userColor = e.target.value

    setItemSelected($(e.target))
})

// reselect color-item
function setItemSelected($item) {
    $('.color-item,.color-item-picker').removeClass('selected')
    $item.addClass('selected')
}

// enter send
$('#msg').keypress((e) => {
    if (e.which == 13) {
        sendMsg()
    }
});

// beforeunload
window.onbeforeunload = function() {
    socket.emit('userleave', { userName, userGroup })
};

// new msg notification
function notifyNewMsg() {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
      console.log("This browser does not support desktop notification");
    }
  
    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      var notification = new Notification("You have new message!");
      setTimeout(()=>notification.close(), 2000)
    }
  
    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
          var notification = new Notification("You have new message!");
          setTimeout(()=>notification.close(), 2000)
        }
      });
    }
  }