// TODO UML, record message channal type of soket
const fs = require('fs');
var express = require('express')
var app = express()
var server = require('http').createServer(app)
var port = process.env.PORT || 3000
var publicDir = __dirname + '/public'
const visitCountFilePath = __dirname + '/data/count-visit.txt'

app.use(express.static(publicDir))

app.all('*', function (req, res) {
  res.sendFile('/index.html', { root: publicDir })
})

/**
 * {
 *  roomId: [userName1,userName2]
 * }
 */
let users = {}
let totalVisited

var io = require('socket.io')(server)
io.on('connection', function (socket) {
  console.log('The page is visited.')
  // visited
  updateVisitCount()
  socket.emit('totalVisited', totalVisited);

  // 1. registerUser  
  socket.on('registerUser', function (data) {
    console.log('New user registed.')
    socket.join(data.userGroup)
    io.to(data.userGroup).emit('userjoin', data)
  })

  // 2. chat message
  socket.on('chatmessage', function (data) {
    io.to(data.userGroup).emit('chatmessage', data)
  })

  socket.on('disconnect', function (data) {
    console.log('A client disconnect.')
  })

  socket.on('userleave', function (data) {
    console.log('A user left.')
    io.to(data.userGroup).emit('userleave', data)
  })
})

server.listen(port, function () {
  // create visit count file if not exist
  fs.existsSync(visitCountFilePath, exists => {
    if (!exists) {
      fs.writeFile(visitCountFilePath, 0)
    }
  })
  console.log(`Server started successfully. http://localhost:${port}`)
})

function updateVisitCount() {
  const content = fs.readFileSync(visitCountFilePath, 'utf-8')
  totalVisited = Number(content) + 1
  fs.writeFileSync(visitCountFilePath, totalVisited)
}